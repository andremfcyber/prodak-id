<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Prodak | Digital Broadcast System</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet"> 
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
  <style type="text/css">
  	html, body {
	    max-width: 100%;
	    overflow-x: hidden;
	} 
	::-webkit-scrollbar {
	  width: 5px;
	  height: 5px;
	}

	/* Track */
	::-webkit-scrollbar-track {
	  background: #f1f1f1; 
	}
	 
	/* Handle */
	::-webkit-scrollbar-thumb {
	  background: #888; 
	}

	/* Handle on hover */
	::-webkit-scrollbar-thumb:hover {
	  background: #555; 
	}
  </style>
  <!-- =======================================================
  * Template Name: Appland - v2.3.0
  * Template URL: https://bootstrapmade.com/free-bootstrap-app-landing-page-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
  <style type="text/css">
  	.logo-navbar {
  		position: relative;    padding: 0px!important;
  	}
  </style>
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top  header-transparentx">
    <div class="container d-flex align-items-center">

      <div class="logo mr-auto">
        <h1 class="text-light">
        	<a href="index.html"> 
        		<img src="assets/img/prodakid.png" class="logo-navbar img-responsive" alt="logo"   /> 
        	</a>
        </h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

      <nav class="nav-menu d-none d-lg-block">
        <ul>
         <!--  <li class="active"><a href="index.html">Home</a></li>
          <li><a href="#features">App Features</a></li>
          <li><a href="#gallery">Gallery</a></li>
          <li><a href="#pricing">Pricing</a></li>
          <li><a href="#faq">F.A.Q</a></li>
          <li><a href="#contact">Contact Us</a></li> -->

          <li class="get-started"><a href="#features">Get Started</a></li>
        </ul>
      </nav><!-- .nav-menu -->

    </div>
  </header><!-- End Header -->
 
<section id="hero" class="d-flex align-items-center" style="padding: 66px 0 1px 0!important;"> 
    <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
	  <div class="carousel-inner">
	    <div class="carousel-item active">
	      <img src="assets/img/slider.jpg" class="d-block w-100" alt="Slider">
	    </div> 
	  </div>
	  <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
	    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
	    <span class="sr-only">Previous</span>
	  </a>
	  <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
	    <span class="carousel-control-next-icon" aria-hidden="true"></span>
	    <span class="sr-only">Next</span>
	  </a>
	</div> 
  </section> 

  <main id="main">
    <!-- ======= App Features Section ======= -->
     <!-- ======= Client Section ======= -->
    <section id="gallery" class="gallery">
      <div class="container">

        <div class="section-title">
          <h2>Trusted by companies like</h2> 
        </div>
        <style>
        	.client-carousel.owl-drag .owl-item {
			    -ms-touch-action: pan-y;
			    touch-action: pan-y;
			    -webkit-user-select: none;
			    -moz-user-select: none;
			    -ms-user-select: none;
			    user-select: none;
			    padding: 26px;
			}
        </style>
    	<div class="owl-carousel client-carousel" data-aos="fade-up">
          <a href="assets/img/client/client1.png" class="venobox" data-gall="client-carousel"><img src="assets/img/client/client1.png" alt=""></a>
          <a href="assets/img/client/client2.png" class="venobox" data-gall="client-carousel"><img src="assets/img/client/client2.png" alt=""></a>
          <a href="assets/img/client/client3.png" class="venobox" data-gall="client-carousel"><img src="assets/img/client/client3.png" alt=""></a>
          <a href="assets/img/client/client4.png" class="venobox" data-gall="client-carousel"><img src="assets/img/client/client4.png" alt=""></a>
          <a href="assets/img/client/client5.png" class="venobox" data-gall="client-carousel"><img src="assets/img/client/client5.png" alt=""></a> 
        </div>

      </div>
    </section><!-- End Client Section -->

    <section id="features" class="features">
      <div class="container">

        <div class="section-title">
          <h2><b>Produk Kami : </b></h2>
          <p>The rise of mobile dive transforms the way. we consume information entriely  <br/>
          	and the worils most elevant channels such as Facebook.
          </p>
        </div>
 
      </div>
    </section><!-- End App Features Section --> 

    <!-- ======= Details Section ======= -->
    <section id="details" class="details">
      <div class="container">

      	 <div class="row content">
          <div class="col-md-6" data-aos="fade-right">
            <img src="assets/img/sec/sec1.png" class="img-fluid" alt="">
          </div>
          <div class="col-md-6 pt-4" data-aos="fade-up">
            <h3>WhatsApp Broadcast & Chatbot.</h3>
            <p class="font-italic">
              Kirim Broadcash WhatsApp ke banyak nomor hanya dengan satu kali klik semua <br/>
              pesan akan dikirim secara otomatis otomatis . 
              
            </p>
            <br/>
            <h5><b>Fitur WhatsApp Chatsbot : </b></h5>
            <div class="row">
            	<div class="col-lg-6">
        		  	<ul>
		              <li><i class="fa fa-circle"></i> Personalitasi.</li>
		              <li><i class="fa fa-circle"></i> Templating.</li>
		              <li><i class="fa fa-circle"></i> Kirim Media.</li>
		              <li><i class="fa fa-circle"></i> Bulk Broadcast.</li>
		              <li><i class="fa fa-circle"></i> One Time Password (OTP).</li>
		           </ul>
            	</div>
            	<div class="col-lg-6">
        			<ul>
		              <li><i class="fa fa-circle"></i> Bisa komunikasi ke servis lain.</li>
		              <li><i class="fa fa-circle"></i> Bisa sebagai gateway helpdesk.</li>
		              <li><i class="fa fa-circle"></i> Schedule Reminder.</li>
		              <li><i class="fa fa-circle"></i> One Time Password (OTP).</li>
		              <li><i class="fa fa-circle"></i> BILLING ( Penagihan ) </li>
		           </ul>
            	</div>
            </div>
           
          </div>
        </div>

      	<!-- / Section --> 
        <div class="row content">
          <div class="col-md-6 order-1 order-md-2" data-aos="fade-left">
            <img src="assets/img/sec/sec2.png" class="img-fluid" alt="">
          </div>
          <div class="col-md-6 pt-5 order-2 order-md-1" data-aos="fade-up">
            <h3>Mobile Collection</h3>
            <p class="font-italic">
              Sebuah aplikasi Cellector atau Account Officer yang berbasis <br/> smartphone, untuk 
              memudahkan petugas BMT untuk penatatan transaksi
            </p>
            <br/>
           <h4><b>Fitur Mobile Collection: </b></h4>
            <div class="row">
            	<div class="col-lg-6">
        		  	<ul>
		              <li><i class="fa fa-circle"></i> List Nasabah</li>
		              <li><i class="fa fa-circle"></i> Pinjam </li>
		              <li><i class="fa fa-circle"></i> Aangsuran</li>
		              <li><i class="fa fa-circle"></i> Inventory Collect</li>
		           </ul>
            	</div>
            	<div class="col-lg-6">
        			<ul> 
		              <li><i class="fa fa-circle"></i> Penagihan</li>
		              <li><i class="fa fa-circle"></i> Reminders</li> 
		              <li><i class="fa fa-circle"></i> Laporan</li>
		           </ul>
            	</div>
            </div>
          </div>
        </div>

         <div class="row content">
          <div class="col-md-6" data-aos="fade-right">
            <img src="assets/img/sec/sec3.png" class="img-fluid" alt="">
          </div>
          <div class="col-md-6 pt-4" data-aos="fade-up">
            <h3>Ecash ( Electronic Cast )</h3>
            <p class="font-italic">
              Kirim Broadcash WhatsApp ke banyak nomor hanya dengan satu kali klik semua <br/>
              pesan akan dikirim secara otomatis otomatis
            </p>
            <br/>
            <h4><b>Fitur Ecast (Electronic Cast) </b></h4>
            <div class="row mt-3">
            	<div class="col-lg-6">
        		  	<ul>
		              <li><i class="fa fa-circle"></i> Fitur 1</li>
		              <li><i class="fa fa-circle"></i> Fitur 2</li>
		              <li><i class="fa fa-circle"></i> Fitur 3</li>
		              <li><i class="fa fa-circle"></i> Fitur 4</li>
		              <li><i class="fa fa-circle"></i> Fitur 5</li>
		           </ul>
            	</div>
            	<div class="col-lg-6">
        			<ul>
		              <li><i class="fa fa-circle"></i> Fitur 1</li>
		              <li><i class="fa fa-circle"></i> Fitur 2</li>
		              <li><i class="fa fa-circle"></i> Fitur 3</li>
		              <li><i class="fa fa-circle"></i> Fitur 4</li>
		              <li><i class="fa fa-circle"></i> Fitur 5</li>
		           </ul>
            	</div>
            </div>
           
          </div>
        </div>

        <!-- / Section --> 
        <div class="row content">
          <div class="col-md-6 order-1 order-md-2" data-aos="fade-left">
            <img src="assets/img/sec/sec4.png" class="img-fluid" alt="">
          </div>
          <div class="col-md-6 pt-5 order-2 order-md-1" data-aos="fade-up"> 
            <p class="font-italic">
              Layana di perusahaan kami untuk client, kami memberikan <br>
              layanan sebagai berikut ini
            </p>
            <br/> 
            <div class="row">
            	<div class="col-lg-6 mb-3">
        		  	<p><img src="assets/img/ico/ico1.png" class="img-fluid" alt="" width="30%"></p> 
	              	<b>App Development</b>
	              	<p>Development aplikasi yang <br>
	              	terstruktur membuat aplikasi dapat <br>
	              	dimaksimalkan
	              	</p>
            	</div>	
            	<div class="col-lg-6 mb-3">
        			<p><img src="assets/img/ico/ico2.png" class="img-fluid" alt="" width="30%"></p> 
	              	<b>UX Planing</b>
	              	<p>Aplikasi dapat dengan mudah digunakan oleh user
	              	</p>
            	</div>
            	<div class="col-lg-6 mb-3">
        			<p><img src="assets/img/ico/ico3.png" class="img-fluid" alt="" width="30%"></p> 
	              	<b>Customer Support</b>
	              	<p>Melayani Support dengan Respon terbaik serta memproritaskan Customer
	              	</p>
            	</div>
            </div>
          </div>
        </div>



	    <!-- ======= Pricing Section ======= -->
	    <section id="pricing" class="pricing">
	      <div class="container">

	        <div class="section-title">
	         	<div class="row justify-content-center">
	         		<div class="col-md-8">
	         			 <h2><b>Dapatkan semua fitur kami dengan cara Belangganan</b></h2>
	         			 <p>Berikut ini adalah list harga berlangganan dari setiap aplikasi yang kami sediakan untuk anda nikmati berlangan bisa perbuatan atau pertahun</p>
	         		</div>
	         	</div>
	         	<br/>
	         	<div class="btn-group btn-group-toggle" data-toggle="buttons">
				  <label class="btn btn-secondary active">
				    <input type="radio" name="options" id="option1" checked> Monthly
				  </label>
				  <label class="btn btn-secondary">
				    <input type="radio" name="options" id="option2"> Years
				  </label> 
				</div>
	        </div>

	        <div class="row no-gutters">

	          <div class="col-lg-4 box" data-aos="fade-right">
	            <h3>Free</h3>
	            <h4>$0<span>per month</span></h4>
	            <ul>
	              <li><i class="bx bx-check"></i> Quam adipiscing vitae proin</li>
	              <li><i class="bx bx-check"></i> Nec feugiat nisl pretium</li>
	              <li><i class="bx bx-check"></i> Nulla at volutpat diam uteera</li>
	              <li class="na"><i class="bx bx-x"></i> <span>Pharetra massa massa ultricies</span></li>
	              <li class="na"><i class="bx bx-x"></i> <span>Massa ultricies mi quis hendrerit</span></li>
	            </ul>
	            <a href="#" class="get-started-btn">Get Started</a>
	          </div>

	          <div class="col-lg-4 box featured" data-aos="fade-up">
	            <h3>Business</h3>
	            <h4>$29<span>per month</span></h4>
	            <ul>
	              <li><i class="bx bx-check"></i> Quam adipiscing vitae proin</li>
	              <li><i class="bx bx-check"></i> Nec feugiat nisl pretium</li>
	              <li><i class="bx bx-check"></i> Nulla at volutpat diam uteera</li>
	              <li><i class="bx bx-check"></i> Pharetra massa massa ultricies</li>
	              <li><i class="bx bx-check"></i> Massa ultricies mi quis hendrerit</li>
	            </ul>
	            <a href="#" class="get-started-btn">Get Started</a>
	          </div>

	          <div class="col-lg-4 box" data-aos="fade-left">
	            <h3>Developer</h3>
	            <h4>$49<span>per month</span></h4>
	            <ul>
	              <li><i class="bx bx-check"></i> Quam adipiscing vitae proin</li>
	              <li><i class="bx bx-check"></i> Nec feugiat nisl pretium</li>
	              <li><i class="bx bx-check"></i> Nulla at volutpat diam uteera</li>
	              <li><i class="bx bx-check"></i> Pharetra massa massa ultricies</li>
	              <li><i class="bx bx-check"></i> Massa ultricies mi quis hendrerit</li>
	            </ul>
	            <a href="#" class="get-started-btn">Get Started</a>
	          </div>

	        </div>

	      </div>
	    </section><!-- End Pricing Section --> 
        <div class="row content">
          <div class="col-md-6" data-aos="fade-right" style="padding: 0px!important;">
            <img src="assets/img/sec/sec5.png" class="img-fluid" alt="">
          </div>
          <div class="col-md-6 pt-5" data-aos="fade-up">
            <h3>Respon Pelanggan yang sudah mengunakan aplikasi kami</h3> 
            <div class="section-title text-left">
	          <h4>Nama PElanggan</h4>
	          <h4><b>Jabatan pelanggan</b></h4>
	          <p>Bring to the table win-win survival strategies to ensure proactive domination. At the end of the day. going forward a new normal that has evolved from generation X is on the runway heading towards a streamined cloud solution. User generated content in real-time will multiple touchpoints for affsharing.</p>
	        </div>
          </div>
        </div>
 		 
      </div>
    </section><!-- End Details Section -->

    
    <section id="faq" class="faq section-bg">
      <div class="container">

        <div class="section-title">

          <h2>Pertanyan yang sering diajukan</h2>
          <p>The rise of mobile device transform the way we costume information entirely <br/>
           and the worlds most elevant channels such as Facebook.</p>
        </div>

        <div class="accordion-list">
          <ul>
            <li data-aos="fade-up">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" class="collapse" href="#accordion-list-1">How to contact with riders emergency? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="accordion-list-1" class="collapse show" data-parent=".accordion-list">
                <p>
                 	Leverage agile frameworks to provide a rabust synopsis for high level everviews, interative approaches to corporate, strategy faster collaborative thinking to further the overall value proposition. Organically grow the hollisic world view of 
                 	disruptive innovation via workplace diversity and empowerment. <br>
                 	Bring to the table win-win survival strategies to ensure proactive domination. at the end the day. going forward. a new
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="100">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#accordion-list-2" class="collapsed">App installation failed, how to update system information? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="accordion-list-2" class="collapse" data-parent=".accordion-list">
                <p>
                  Dolor sit amet consectetur adipiscing elit pellentesque habitant morbi. Id interdum velit laoreet id donec ultrices. Fringilla phasellus faucibus scelerisque eleifend donec pretium. Est pellentesque elit ullamcorper dignissim. Mauris ultrices eros in cursus turpis massa tincidunt dui.
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="200">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#accordion-list-3" class="collapsed">Website response taking time, how to improve? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="accordion-list-3" class="collapse" data-parent=".accordion-list">
                <p>
                  Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Faucibus pulvinar elementum integer enim. Sem nulla pharetra diam sit amet nisl suscipit. Rutrum tellus pellentesque eu tincidunt. Lectus urna duis convallis convallis tellus. Urna molestie at elementum eu facilisis sed odio morbi quis
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="300">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#accordion-list-4" class="collapsed"> New update fixed all bug and issues <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="accordion-list-4" class="collapse" data-parent=".accordion-list">
                <p>
                  Molestie a iaculis at erat pellentesque adipiscing commodo. Dignissim suspendisse in est ante in. Nunc vel risus commodo viverra maecenas accumsan. Sit amet nisl suscipit adipiscing bibendum est. Purus gravida quis blandit turpis cursus in.
                </p>
              </div>
            </li>

            <li data-aos="fade-up" data-aos-delay="400">
              <i class="bx bx-help-circle icon-help"></i> <a data-toggle="collapse" href="#accordion-list-5" class="collapsed">Now to contact with riders emergency? <i class="bx bx-chevron-down icon-show"></i><i class="bx bx-chevron-up icon-close"></i></a>
              <div id="accordion-list-5" class="collapse" data-parent=".accordion-list">
                <p>
                  Laoreet sit amet cursus sit amet dictum sit amet justo. Mauris vitae ultricies leo integer malesuada nunc vel. Tincidunt eget nullam non nisi est sit amet. Turpis nunc eget lorem dolor sed. Ut venenatis tellus in metus vulputate eu scelerisque.
                </p>
              </div>
            </li>

          </ul>
        </div>

      </div>
    </section>  
  </main> 
 
  <footer id="footer" style="padding: 0px!important;"> 

  	<div class="bg-foot">
  		<div class="row">
  			<div class="col-lg-6"> 
  				<div class="row justify-content-center">
  					<div class="col-md-8">
  						<div class="section-title text-left p-5"> 
				          <h2>Get Application</h2>
				          <p>The rise of mobile device transform the way we costume information entirely <br/>
				           and the worlds most elevant channels such as Facebook.</p>
				        </div> 
  					</div>
  				</div>	
  			</div>
  			<div class="col-lg-6">
            	<img src="assets/img/sec/foot.png" class="img-fluid" alt=""> 
  			</div>
  		</div>
  	</div>
   
    <div class="p-5" style="background: #09152f;">
      <div class="copyright">
        &copy; Copyright 
        	<strong>
        		<span>logiaedu
				</span>
			</strong>. All Rights Reserved
      </div>
      <div class="credits"> 
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>
    </div>
  </footer>


  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>